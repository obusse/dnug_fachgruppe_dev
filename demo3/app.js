// webserver stuff
var express = require('express');
var app = express();

// templating engine
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

// domino-db
const {
    useServer
} = require('@domino/domino-db');

// proton config
const serverConfig = {
    "hostName": "mardou.dyndns.org",
    "connection": {
        "port": 3002
    }
}

// domino nsf
const databaseConfig = {
    "filePath": "fakenames.nsf"
};

const query = "Form = 'Person' and firstname in ('John', 'Peter')";

function getNames(callback) {
    useServer(serverConfig).then(async server => {
        const database = await server.useDatabase(databaseConfig);
        const result = await database.bulkReadDocuments({
            query: query,
            count: 100,
            itemNames: ["lastname", "firstname"]
        });
        console.log(result);
        // documents is the field that contains the array of found documents
        callback(result.documents, result.documentRange.total, result.documentRange.count);
    }).catch(error => {
        console.log("%s. Cause: %s", error.message, error.code);
    });
}

app.get('/', function (req, res) {
    getNames(function (result, total, count) {
        res.render('index', {
            title: "Results",
            query: query,
            total: total,
            count: count,
            documents: result
        });
    });
});

// start server
var server = app.listen(3000, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("Example app listening at http://%s:%s", host, port)
})