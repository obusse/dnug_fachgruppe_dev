// domino-db
const {
    useServer
} = require('@domino/domino-db');

// proton config
const serverConfig = {
    "hostName": "mardou.dyndns.org",
    "connection": {
        "port": 3002
    }
}

// domino nsf
const databaseConfig = {
    "filePath": "fakenames.nsf"
};

const query = "Form = 'Person' and LastName = 'Smith' and FirstName = 'Pat'";

function init() {
    useServer(serverConfig).then(async server => {
        const database = await server.useDatabase(databaseConfig);
        const documents = await database.bulkReadDocuments({
            query: query,
            itemNames: ["lastname", "firstname"]
        });
        console.log("====================================================");
        console.log(documents);
        const explain = await database.explainQuery({
            query: query
        });
        console.log("====================================================");
        console.log(explain);
    }).catch(error => {
        console.log("%s. Cause: %s", error.message, error.code);
    });
}

// start the query
init();