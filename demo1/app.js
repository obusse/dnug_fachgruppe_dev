// domino-db
const {
    useServer
} = require('@domino/domino-db');

// proton config
const serverConfig = {
    "hostName": "mardou.dyndns.org",
    "connection": {
        "port": 3002
    }
}

// domino nsf
const databaseConfig = {
    "filePath": "fakenames.nsf"
};

function init() {
    useServer(serverConfig).then(async server => {
        const database = await server.useDatabase(databaseConfig);
        // view?
        console.log(database);
    }).catch(error => {
        console.log("%s. Cause: %s", error.message, error.code);
    });
}

// start the query
init();