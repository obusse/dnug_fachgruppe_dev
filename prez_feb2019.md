<!-- MARP file -->
<!-- $theme: default -->
<!-- $size: A4 -->

<style>
  * {
  font-size: 95%;
  }
</style>
<img src="images/DNUG_LOGO_4.png"/>

<div style="float:right;text-align:right">
<img style="width:200px;" src="images/obusse2018_1sw.png"/>
  <p>Oliver Busse</p>
<span style="font-size:0.7em">We4IT GmbH</span>
<br/>
<a href="https://aveedo.com"><img src="images/aveedo-logo.png" style="width:200px"/></a>
  <br/>
  <span style="font-size:0.7em">
<a href="http://aveedo.com">aveedo.com</a>
</span>
<br/>
<span style="font-size:0.7em">
<a href="https://twitter.com/zeromancer1972">@zeromancer1972</a></span>
<br/>
  <span style="font-size:0.7em">
<a href="http://oliverbusse.com">oliverbusse.com</a>
</span>
<br/>
  <span style="font-size:0.5em">2015-2018</span>
  <br/>
<img src="images/ibm-champion-rgb-175px.jpg"/>
  </div>
  <div style="position:relative;top:60px;">
  <h1>Node.js &amp; <br/>Domino Query Language</h1><br/><br/>
  <h3>DNUG Fachgruppentag Development</h3>
  <h4>Essen, 20. Februar 2019</h4>
  </div>

---

<!-- page_number: true -->

# Agenda

- Was ist Node.js?
- Node.js in Domino
- Sinn &amp; Zweck, H&uuml;rden
- Meine erste Node App mit Domino
- Domino Query Language (DQL)
- Baustellen
- Low-Code
- Fazit
- Community

---

<!-- page_number: true -->

# Was ist Node.js?

- Server-Side Javascript Plattform
- Basierend auf Googles Javascript V8 Engine
- Non-blocking Thread -> skalierbar
- verf&uuml;gbar f&uuml;r alle Systeme (Windows, Linux, Mac, ARM, ...)
- Asynchron
- Package Management
- weit verbreitet

---

<!-- page_number: true -->

# Node.js in Domino

- Domino 10.0.1
- Domino App Dev Pack *(Linux)*
- PROTON Task
- gRPC (https://grpc.io/about/)
- IAM (Identity Access Management) - Service &amp; Client *(preview)*
- kein Bestandteil von DDE!

---

<!-- page_number: true -->

# Node.js in Domino - Schema

<img src="images/proton_schema.png"/>

---

<!-- page_number: true -->

# Proton Setup (1)

- Proton Addin auf Server kopieren

`cd /opt/ibm/domino/notes/latest/linux`

`sudo tar -xvf /tmp/proton-addin-<version>.tgz`

- ggf. Certifier und Keyring erzeugen (f&uuml;r IAM)
- notes.ini Eintr&auml;ge einf&uuml;gen

```plaintext
# listen on all ip addresses, otherwise define machine's ip
PROTON_LISTEN_ADDRESS=0.0.0.0
# listen on port - free to choose
PROTON_LISTEN_PORT=3002
# use SSL
PROTON_SSL=1
# client certificate authorization, default: anonymous
PROTON_AUTHENTICATION=client_cert
# keyfile, can be the keyfile of SSL enabled HTTP task
PROTON_KEYFILE=proton.kyr
```
---

<!-- page_number: true -->

# Proton Setup (2)

- Task automatisch mitstarten

```plaintext
ServerTasks=Replica,Router,Update,AMgr,
	Adminp,Sched,CalConn,RnRMgr,HTTP,proton
```

- oder manuell

`load proton`

<img src="images/proton_console.png"/>

---

<!-- page_number: true -->

# Sinn & Zweck, H&uuml;rden

- Erg&auml;nzt bzw. ersetzt DAS
- Entwicklung von Web-Applikationen jenseits XPages m&ouml;glich
- Cloud-ready
- Domino ist damit offen f&uuml;r Fullstack Web Developer
- Nachwuchs?
- Setup sehr komplex (IAM, Domino als OAuth Provider)
- Unfertig (Stand Feb. 2019)
	- es fehlen Assistenten/Scripts f&uuml;r Admins &amp; Entwickler
	- Dokumentation vorhanden, jedoch "Luft nach oben"

---

<!-- page_number: true -->

# Meine erste Node App mit Domino

<img src="images/node_js.png" align="right" style="width:20%;height:20%"/>

- Node.js
- npm
- domino-db Package
- Proton up &amp; running
- Editor

---

<!-- page_number: false -->

<img src="images/demo.jpg"/>

demo1

---

<!-- page_number: true -->

# Domino Query Language (DQL)

- Datenbanken vorbereiten -> Design catalog
	- `load updall <database> -e` (enable)
    - `load updall <database> -d` (refresh)
- `UPDATE_DESIGN_CATALOG=1`
- Vef&uuml;gbar f&uuml;r Node.js, Java, Lotusscript &amp; Konsole *\**
- Java &amp; LS: gleicher Server oder lokale Replik (10er Client), allerdings nicht gemischt
- Vereinfacht: kombiniert Vorteile von View Index &amp; FT Search
- Sehr schnell

<small>*\*) zum Testen gut, liefert aber keine verwertbare Ergebnisse*</small>

---

<!-- page_number: true -->

# DQL - Sprachunterschiede

- Node.js
```javascript
const docs = await database.bulkReadDocuments({
	query: "form = 'person' and lastname in ('Smith', 'Douglas')",
    itemNames: ["lastname", "firstname"]
});

```
- Java
```java
DominoQuery dq = database.createDominoQuery();
DocumentCollection col = 
dq.execute("form = 'person' and lastname in ('Smith', 'Douglas')");
```
- Lotusscript
```vbnet
Dim dq As NOTESDOMINOQUERY
Set dq = database.CreateDominoQuery
Dim col As NotesDocumentCollection
Set col = _
	dq.Execute(|form = 'person' and lastname in ('Smith', 'Douglas')|)
```

---

<!-- page_number: true -->

# DQL - Suchoptionen

- Suche nach Einzelwerten
	- `form = 'person'`
- Suche nach Mehrfachwerten
	- `lastname in ('Smith', 'Doe')`
- Suche nach Datum und Datumsbereichen
	- `@dt('2019-02-20') > lastModified > @dt('2019-01-01')`
- Suche nach Werten und Wertebereichen
	- `45 > age > 19`
- Operatoren
	- `= | > | >= | < | <= | in [all]`
	- `and | or | and not | or not`
---

<!-- page_number: true -->

# DQL - Strategien

- Nutzen der "Explain" Funktion
- testet nur, f&uuml;rt nichts aus

```plaintext
Query Processed:
[form = 'post' and postTags in ('dnug')]

0. AND   	 (childct 2) (totals when complete:) 
	Prep 0.0 msecs, Exec 12.30 msecs, ScannedDocs 5, 
	Entries 0, FoundDocs 5
    
	1.form = 'post' NSF document search estimated cost = 100  
		Prep 2.274 msecs, Exec 0.758 msecs, ScannedDocs 5
		Entries 0, FoundDocs 5
	1. IN (childct 1) (totals when complete:)
		Prep 0.0 msecs, Exec 11.259 msecs, ScannedDocs 802, 
        Entries 0, FoundDocs 5
		2.postTags = 'dnug' NSF document search 
		estimated cost = 100  
		Prep 0.201 msecs, Exec 11.180 msecs, 
        ScannedDocs 802, Entries 0, FoundDocs 5
```
---

<!-- page_number: false -->

<img src="images/demo.jpg"/>

demo2 &amp; demo3

---

<!-- page_number: true -->

# Baustellen (Feb 2019)

<img src="images/under_construction.jpg" align="right"/>

- kein Richtext/MIME und Attachments (Node.js)
	- weder lesend noch schreibend
- Felder, die im Resultset enthalten sein sollen, m&uuml;ssen definiert werden (Node.js)
	- GraphQL Standard und grunds&auml;tzlich gut, aber es fehlt eine Option f&uuml;r "alle Felder"
- Dokumentation (IAM, OAuth)
	- teilweise verwirrend und nicht eindeutig
- Proton Task schreibt nichts in die log.nsf
	- durch notes.ini-Settings aber auf der Konsole "verbose" zu schalten
- Proton bisher nur f&uuml;r Linux
	- noch kein Termin f&uuml;r Windows bekannt

---

<!-- page_number: true -->

<img src="images/node-red-icon.png" style="width:20%;height:20%" align="right"/>

# Low-Code

- Basis f&uuml;r alles rund um IoT
- Think 2019 Demos von Luis Guirigay mit Alexa sind mit Node-RED gebaut
- visuelles Programmieren mit Javascript (nicht Node.js!)
- l&auml;uft selbst auf einem Raspberry Pi :-)
- Plugins ohne Ende
	- Domino V10 & DQL
	- IBM Watson Services
	- Amazon Alexa

---

<!-- page_number: false -->

<img src="images/demo.jpg"/>

Node-RED

---

<!-- page_number: true -->

# Fazit

- Node.js / Proton ist momentan ein PoC
- Auf DQL haben wir lange gewartet
- Low-Code mit Node-RED

---

<!-- page_number: true -->

<img src="images/openntf.jpg" style="width:50%" align="right"/>

# Community

- Hilfe gibt es auf https://openntf.slack.com im ***dominonodejs*** Channel

---

<!-- page_number: true -->

# Quellen

- Webinar DQL &amp; FAQ: https://www.ibm.com/blogs/collaboration-solutions/2019/02/04/domino-query-language-faq
- Domino AppDevPack Documentation: https://doc.cwpcollaboration.com/appdevpack/docs/en/homepage.html
- Quick Overview: https://prominic.net/2018/07/02/node-js-domino-v10-essentials
- Node-RED: https://nodered.org/
- Node-RED DominoV10 Nodes: https://github.com/stefanopog/node-red-contrib-dominodb
- Vortrags-Repository: https://gitlab.com/obusse/dnug_fachgruppe_dev

---

<!-- page_number: false -->

<img src="images/thanks.jpg"/>

---